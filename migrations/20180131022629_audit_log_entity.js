
exports.up = async function(knex, Promise) {
  const sch = knex.schema;
  await sch.createTable('audit_log_entity', t => {
    t.integer('event_id').primary();
    t.foreign('event_id').references('audit_log_event.id');
    t.string('entity');
  })
};

exports.down = async function(knex, Promise) {
  const sch = knex.schema;
  await sch.dropTable('audit_log_entity')
};
