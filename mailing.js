const nodemailer = require('nodemailer');

let smtpConfig = {
  host: 'smtp.seznam.cz',
  port: 465,
  secure: true, // upgrade later with STARTTLS
  auth: {
      user: 'ita.react.2018@email.cz',
      pass: '151-051-261',
  },
  tls: { 
    rejectUnauthorized: false,
  },
  logger: true,
};




let transporter = nodemailer.createTransport(smtpConfig);

async function sendPasswordRecoveryEmail(userEmail, link) {
  let message = {
    from: 'ita.react.2018@email.cz',
    to: userEmail, 
    subject: 'ITA React App - Password reset',
    text: `Hello, your email address was used to ask for a password recovery. Please navigate ${link} in your web browser.`,
    html: `Hello, your email address was used to ask for a password recovery. <a href="${link}">CLICK HERE</a> to reset your password.`,
  };    
  await transporter.sendMail(message);
}

module.exports = {
  sendPasswordRecoveryEmail,
}

