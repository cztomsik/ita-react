/*
  Immutably changes a value in a nested structure.
  Could be a four-liner, but this form could bring better understanding
*/
export function setIn(immutableObject, path, value) {
  // We are gonna set following key in current immutableObject structure
  const currentKeyName = path[0];
  // This is an old content stored under this key. Creates a new object when the old one does not exist. Does not handle creating arrays :( so far...
  const innerContent = immutableObject[currentKeyName] || {};
  // This is a rest of the path to resolve
  const pathRest = path.slice(1);
  // Finally a new content for the key
  let newInnerContent;
  if(path.length > 1) {
    // There will be some more path points to resolve - go one level deeper.
    newInnerContent = setIn(innerContent, pathRest, value);
  } else {
    // This path is the last one. Use the value...
    newInnerContent = value;
  }
  return {
    ...immutableObject,
    [currentKeyName]: newInnerContent
  }
}

export function getIn(obj, path, defaultValue) {
  if(!obj) return defaultValue;
  if(obj.hasOwnProperty(path[0])) {
    if(path.length > 1) {
      return getIn(obj[path[0]], path.slice(1), defaultValue);
    } else {
      return obj[path[0]];
    }
  } else {
    return defaultValue
  }
}

