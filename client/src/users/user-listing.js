import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

class UserListing extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      users: []
    }
  }

  componentWillMount() {
    this.load()
  }

  load() {
    usersService.getUsers().then(res => {
      this.setState({
        users: res.data
      })
    })
  }

  render() {
    const users = this.state.users

    return (
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users</h3>
            </div>
            <div class="box-body">
              <div className="row">
                <div className="col-sm-12">
                  <table className="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Login</th>
                        <th>Name</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users.map(u =>
                        <tr key={u.id}>
                          <td>
                          <Link to={ROUTES.getUrl(ROUTES.USER_DETAIL, { id: u.id })}>{u.login}</Link>
                          </td>
                          <td>{u.name}</td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                  <div>
                    <Link to={ROUTES.USER_NEW} className="btn btn-default">Create new</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


export default UserListing
