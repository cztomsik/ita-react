import React from 'react'


const UserForm = ({ user, handleChange }) => {

  return (
    <form>
      <div className="form-group">
        <label>Login</label>
        <input name="login" type="text" className="form-control" value={user.login} onChange={handleChange} />
      </div>
      <div className="form-group">
        <label>Name</label>
        <input name="name" type="text" className="form-control" value={user.name} onChange={handleChange} />
      </div>
    </form>
  )
}

export default UserForm