import http from '../http'

export default {
  getCustomers() {
    return http.get('/customers')
  },

  createCustomer(data) {
    return http.post('/customers', data)
  },

  getCustomer(id) {
    return http.get('/customers/' + id)
  },

  updateCustomer(customer) {
    return http.put('/customers/' + customer.id, customer)
  },

  searchCustomers(searchText) {
    return http.get('/customers/?q=' + searchText)
  }
}
