import React from 'react'

import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Cell } from 'recharts';

const FunnelChart = ({ contactStatusData }) => {
    const colors = ['#FA0027','#8B06DF','#0874DD']

    return (
        <div className="dashboard-count-boxes">
            <div className="box box-solid box-primary" data-widget="box-widget">
                <div className="box-header">
                    <h3 className="box-title">Funnel Chart</h3>
                </div>
                <div className="box-body">
                    <BarChart barCategoryGap={5} width={600} height={300} data={contactStatusData}
                        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                        <XAxis dataKey="status" />
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3" />
                        <Tooltip />
                        <Bar dataKey="count" >
                            {
                                contactStatusData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={colors[index]} />
                                ))
                            }
                        </Bar>
                    </BarChart>
                </div>
            </div>
        </div>
    )

}

export default FunnelChart